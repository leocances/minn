import { getAppDataSource } from "../data-source";
import { Url } from "../entity/url";



export interface IUrl {
    ip?: string;
    hash?: string;
    url?: string;
    bump?: string;
}


export const createUrl = async (url: IUrl) => {
    getAppDataSource()
    .then(dataSource => {
        const _newUrl = new Url();

        if (url.ip) _newUrl.ip = url.ip;
        if (url.hash) _newUrl.hash = url.hash;
        if (url.url) _newUrl.url = url.url;
        if (url.bump) _newUrl.bump = url.bump;
    
        dataSource.manager.save(_newUrl)
    })
}


export const getUrlByHash = async (hash: string) => {
    const dataSource = await getAppDataSource();
    const url = await dataSource.manager.findOne(Url, { 
        where: {hash: hash}
    });
    return url;
}


export const getUrlByUrl = async (url: string) => {
    const dataSource = await getAppDataSource();
    const _url = await dataSource.manager.findOne(Url, { 
        where: {url: url}
    });
    return _url;
}