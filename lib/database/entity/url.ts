import {Entity, PrimaryGeneratedColumn, Column, BaseEntity} from "typeorm";


@Entity('Url')
export class Url extends BaseEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column('text', {nullable: true})
    ip!: string;

    @Column()
    hash!: string;

    @Column()
    url!: string;

    @Column()
    bump!: string;  // in case of collision

    @Column({default: () => 'CURRENT_TIMESTAMP'})
    createdAt!: Date;
}