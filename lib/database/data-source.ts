import "reflect-metadata";
import { DataSource } from "typeorm";
import { join } from "path";
import { Url } from "./entity/url";
import * as dotenv from "dotenv";

dotenv.config({path: join(__dirname, '../../.env.local')});

const appDataSource = new DataSource({
    type: "mysql",
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_PORT || '3306'),
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    synchronize: true,
    logging: false,
    entities: [Url],
    migrations: [],
    subscribers: [],
})

let dataSourceReadyPromise: Promise<void> | null = null;

// initialize the data source if not already initialized
// otherwise return the existing promise
function initDataSource() {
  if (!dataSourceReadyPromise) {
    dataSourceReadyPromise = (async () => {
      // clean up old DS that references outdated hot-reload classes
      if (appDataSource.isInitialized) {
        // await appDataSource.destroy();
        console.log('already exist')
      }
      // wait for new DS
      await appDataSource.initialize();
    })();
  }
}

export const getAppDataSource = async () => {
  if (!appDataSource.isInitialized) {
    await appDataSource.initialize();
  }

  return appDataSource;
};

// export const getAppDataSource = async () => {
//   initDataSource();
//   await dataSourceReadyPromise;
//   return appDataSource;
// };

export default appDataSource;