import type { NextPage } from 'next';
import React from 'react';
import {
  Flex,
  Text,
  Box,
  Input,
  Button,
  IconButton,
  useToast,
  useClipboard,
  Heading,
  Wrap,
  WrapItem
} from '@chakra-ui/react';
import { Footer } from '../components/footer';
import { CopyIcon, CheckCircleIcon } from '@chakra-ui/icons';

const HOST=process.env.NEXT_PUBLIC_HOST;

const Home: NextPage = () => {
  const [input, setInput] = React.useState('');
  const [hash, setHash] = React.useState('');
  const [errorMsg, setErrorMsg] = React.useState('');
  const [working, setWorking] = React.useState(false);

  const { hasCopied, onCopy } = useClipboard(`${HOST}/` + hash);


  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value

    if (value === '') {
      setErrorMsg('');
      return;
    }

    if (value.length >= 4) {
      // check if value start with http:// or https://
      if (!value.startsWith('http://') && !value.startsWith('https://')) {
        setErrorMsg('URL must start with http:// or https://');
        return;
      }
    }

    try {
      new URL(value);
    } catch (e) {
      console.error(e);
      setErrorMsg('Invalid URL');
      return;
    }

    setErrorMsg('');
    setInput(value);
  }


  const buttonHandler = async () => {
    setWorking(true);

    const payload = {
      url: input
    }
    let url = `api/url/push`

    fetch(url, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      setHash(data.hash)
    })
    .catch(err => {
      console.error(err)
    });
    setWorking(false);
  }

  return (
    <Flex direction={'column'} justifyContent={'center'} p={4}>
      <Flex
        direction={'column'}
        maxW={['xl', '2xl', '4xl']}
        mx='auto'
        mt={'10%'}
        h='100vh'
      >
        <Heading
          fontSize={['4xl', '6xl']}
          fontWeight={'black'}
        >
          Minn me
        </Heading>

        <Text ml={8} color={'gray.500'} fontWeight={'bold'}>
          Minify your URLs, simply.
        </Text>

        <Wrap w='full' p={4} mt={8}>
          <WrapItem>
            <Input 
              borderRadius={'full'}
              borderColor={'gray.500'}
              placeholder='url' 
              onChange={(e) => onChangeHandler(e)}
              w={['2xs', 'xs', 'sm']}
            />
          </WrapItem>

          <WrapItem mx={'auto'}>
            <Button
              borderRadius={'full'}
              onClick={buttonHandler}
              isLoading={working}
            >
              Minn me
            </Button>
          </WrapItem>
        </Wrap>
        <Text 
          // h={6}
          ml={8}
          mt={2}
          color={'red.500'}
          fontSize={'md'}
          fontWeight={'bold'}
        >
          {errorMsg}
        </Text>



        <Flex mt={8} mx={'auto'}>
          <Text mr={8}>{HOST}/{hash}</Text>
          <IconButton 
            isLoading={working}
            aria-label='copy'
            size={'xs'}
            icon={hasCopied ? <CheckCircleIcon /> : <CopyIcon />} onClick={onCopy}/>
        </Flex>
      </Flex>
    <Footer />
  </Flex>
  )
}

export default Home
