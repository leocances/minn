import type { NextApiRequest, NextApiResponse } from 'next';
import Adler32 from "adler-32";

import { Url } from '../../../lib/database/entity/url';
import { createUrl, IUrl } from '../../../lib/database/crud/url';


const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method !== 'POST') {
        res.status(405).send({ error: 'Only POST requests allowed' })
        return;
    }

    let urlToSave = req.body.url as string;
    if (!urlToSave) {
        res.status(400).send({ error: 'Missing url' })
        return;
    }

    // hash the url
    const bump = '0';  // TODO: use a real bump
    const hash = Math.abs(Adler32.str(urlToSave + bump));

    // Get the user IP
    let ip;
    if (req.headers["x-forwarded-for"]) {
        ip = req.headers["x-forwarded-for"] as string;
        ip = ip.split(',')[0]
    } else if (req.headers["x-real-ip"]) {
        ip = req.socket.remoteAddress
    } else {
        ip = req.socket.remoteAddress
    }
    console.log('user ip: ', ip)
      
      

    // // store the minified into the database
    let toSave: IUrl = {
        ip: ip,
        hash: hash.toString(16),
        url: urlToSave,
        bump: bump,  // in case of collision
    }
    await createUrl(toSave);
    res.status(200).send({ message: 'OK', hash: hash.toString(16) })
}

export default handler;