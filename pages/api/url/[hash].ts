import type { NextApiRequest, NextApiResponse } from 'next';

import {
    IUrl,
    getUrlByHash,
} from '../../../lib/database/crud/url';


const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const hash = req.query.hash as string;
    if (!hash) {
        res.status(400).send({ error: 'Missing hash' })
        return;
    }

    let url = await getUrlByHash(hash);
    if (!url) {
        // TODO: make a clean 404 pages
        res.status(404).send({ error: 'Url not found' })
        return;
    }

    res.status(200).send({url: url.url})
}

export default handler;