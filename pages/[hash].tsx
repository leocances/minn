import { useRouter } from "next/router";
import { PageNotFound } from "../components/404";
import React from "react";
import {
    Flex
} from "@chakra-ui/react";

const TransitionPage: React.FC = () => {
    const [notFound, setNotFound] = React.useState<boolean>(false);
    const router = useRouter();
    const {hash} = router.query;


    React.useEffect(() => {
        if (!hash) {
            return;
        }

        const url = `api/url/${hash}`;
        fetch(url, {
            method: "GET",
        })
        .then(res => res.json())
        .then(data => {
            if (data.url === undefined) {
                setNotFound(true);
                return;
            }

            window.location.replace(data.url);
        })
        .catch(err => {
            console.error(err);
            setNotFound(true);
          });
    });

    return (
        <Flex>
            {notFound ? <PageNotFound /> : null}
        </Flex>
    )
}

export default TransitionPage;