import { useRouter } from "next/router";
import React from "react";
import {
  Text,
  Button,
  Flex,
} from "@chakra-ui/react";


export const PageNotFound: React.FC = () => {
  const router = useRouter();

  return (
    <Flex direction={'column'} justifyContent="center" alignItems="center" height="100vh" mx={'auto'} p={4}>
      <Text
        fontSize={'8xl'}
        fontWeight={'black'}
        letterSpacing={'.1rem'}
      >
        Oops!
      </Text>

      <Text
        fontSize={'xl'}
        fontWeight={'black'}
        letterSpacing={'.1rem'}
      >
        404 - PAGE NOT FOUND
      </Text>

      <Text mt={8}>
        The page you are looking for might have never been minified.
      </Text>

      <Button mt={8} borderRadius={'full'} >
        <Text p={4} letterSpacing={'.1rem'} onClick={() => router.push('/')}>
          GO TO HOMEPAGE
        </Text>
      </Button>
    </Flex>
  );
}