import React from "react";
import {
  Flex,
  IconButton,
  ButtonGroup,
  Text,
  Spacer,
  Wrap,
  WrapItem,
  Tooltip
} from "@chakra-ui/react";
import { FaLinkedin, FaGitlab } from 'react-icons/fa'

export const Footer: React.FC = () => {
  const [currentYear, setCurrentYear] = React.useState(new Date().getFullYear());


  React.useEffect(() => {
    setCurrentYear(new Date().getFullYear());
  } , []);

  
  return (
    <Flex 
      position={'sticky'}
      bottom={0}
      bg={'white'}
    >
      <Wrap
        minW='full'
        py={2}
        px={4}
        mt={'auto'}
        borderTopWidth={1}
      >
        <Spacer />

        <WrapItem>
          <Text>
            © {currentYear} leocances.fr All rights reserved.
          </Text>
        </WrapItem>

        <Spacer />

        <WrapItem>
          <ButtonGroup variant="ghost">
            <Tooltip label='Linkedin'>
              <IconButton
                as="a"
                href="https://www.linkedin.com/in/leocances/"
                aria-label="LinkedIn"
                icon={<FaLinkedin fontSize="1.25rem" />}
              />
            </Tooltip>

            <Tooltip label='Gitlab'>
              <IconButton as="a" href="https://gitlab.com/leocances/minn" aria-label="Gitlab" icon={<FaGitlab fontSize="1.25rem" />} />
            </Tooltip>
          </ButtonGroup>
        </WrapItem>
      </Wrap>
    </Flex>
  );
}